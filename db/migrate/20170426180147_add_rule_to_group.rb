class AddRuleToGroup < ActiveRecord::Migration
  def change
    add_reference :groups, :rule, index: true
  end
end
