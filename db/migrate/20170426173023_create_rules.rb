class CreateRules < ActiveRecord::Migration
  def change
    create_table :rules do |t|
      t.integer :order
      t.string :action
      t.string :option

      t.timestamps null: false
    end
  end
end
