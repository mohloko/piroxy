class RemoveRuleIdFromGroup < ActiveRecord::Migration
  def change
    remove_column :groups, :rule_id, :integer
  end
end
