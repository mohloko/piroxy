class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :name
      t.string :email
      t.integer :staff
      t.string :proxy_pass
      t.string :password

      t.timestamps null: false
    end
  end
end
