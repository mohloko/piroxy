class RemoveOrderFromRules < ActiveRecord::Migration
  def change
    remove_column :rules, :order, :integer
  end
end
