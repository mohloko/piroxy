class RemoveRuleIdFromCategory < ActiveRecord::Migration
  def change
    remove_column :categories, :rule_id, :integer
  end
end
