class CreateCategorizations < ActiveRecord::Migration
  def change
    create_table :categorizations do |t|
      t.integer :site_id
      t.integer :category_id
      t.datetime :created_at

      t.timestamps null: false
    end
  end
end
