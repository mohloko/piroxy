class AddIdsToRule < ActiveRecord::Migration
  def change
    add_reference :rules, :group, index: true
    add_foreign_key :rules, :groups
    add_reference :rules, :category, index: true
    add_foreign_key :rules, :categories
  end
end
