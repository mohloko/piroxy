class Category < ActiveRecord::Base
  has_many :rule
  has_many :sites, through: :categorizations
end
