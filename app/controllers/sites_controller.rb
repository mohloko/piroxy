class SitesController < ApplicationController
  def index
    @sites = Site.all
    @categories = Category.all
  end
  
  def show
    @site = Site.find(params[:id])
    
  end
  
  def new
    @site = Site.new
  end
  
  def create
    @site = Site.new(site_params)

    if @site.save
      
      flash[:success] = "Usuário cadastrado com sucesso!"
      redirect_to @site
    else
      render 'new'
    end
  end
  
  def edit 
    @site = Site.find(params[:id])
  end
  
  def update
    @site = Site.find(params[:id])
    if @site.update_attributes(site_params)
      flash[:success] = "Perfil atualizado!"
      redirect_to @site
    else
      render 'edit'
    end
  end
  
  def destroy
    Site.find(params[:id]).destroy
    flash[:success] = "Usuário deletado!"
    redirect_to sites_url
  end
  
  private
  
  def site_params
    params.require(:site).permit(:name, :category_ids)
  end

end
