class RulesController < ApplicationController
  
  def index

    @rules = Rule.order("position")
    #@groups = Group.all
  end
  
  def show
    @rule = Rule.find(params[:id])
    
  end
  
  def new
    @rule = Rule.new
  end
  
  def create
    @rule = Rule.new(rule_params)

    if @rule.save
      
      flash[:success] = "Regra cadastrado com sucesso!"
      redirect_to @rule
    else
      render 'new'
    end
  end
  
  def edit 
    @rule = Rule.find(params[:id])
  end
  
  def update
    @rule = Rule.find(params[:id])
    if @rule.update_attributes(rule_params)
      flash[:success] = "Perfil atualizado!"
      redirect_to @rule
    else
      render 'edit'
    end
  end
  
  def destroy
    Rule.find(params[:id]).destroy
    flash[:success] = "Usuário deletado!"
    redirect_to rules_url
  end
  
  def sort
    params[:rule].each_with_index do |id, index|
      Rule.update(id, position: index+1)
    end
  render nothing: true
 end

  private
  
  def rule_params
    params.require(:rule).permit(:name, :action, :order, :option, :group_id, :category_id, :position)
  end
end