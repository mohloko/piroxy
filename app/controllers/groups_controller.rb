class GroupsController < ApplicationController
  def index
    @groups = Group.all
  end
  
  def show
    @group = Group.find(params[:id])
    
  end
  
  def new
    @group = Group.new
  end
  
  def create
    @group	=	Group.new(group_params)

    if @group.save
      
      flash[:success] = "Grupo cadastrado com sucesso!"
      redirect_to @group
    else
      render 'new'
    end
  end
  
  def edit 
    @group = Group.find(params[:id])
  end
  
  def update
    @group = Group.find(params[:id])
    if @group.update_attributes(group_params)
      flash[:success] = "Grupo atualizado!"
      redirect_to @group
    else
      render 'edit'
    end
  end
  
  def destroy
    Group.find(params[:id]).destroy
    flash[:success] = "Grupo deletado!"
    redirect_to groups_url
  end
  
  private
  
  def group_params
    params.require(:group).permit(:name)
  end

end
